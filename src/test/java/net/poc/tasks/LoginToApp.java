package net.poc.tasks;

import net.poc.userinterfaces.LogingPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LoginToApp implements Task {
    public static LoginToApp login(){
        return instrumented(LoginToApp.class);
    }
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(LogingPage.USER),
                SendKeys.of("standard_user").into(LogingPage.USER),
                Click.on(LogingPage.PASSWORD),
                SendKeys.of("secret_sauce").into(LogingPage.PASSWORD),
                Click.on(LogingPage.LOGIN_BTN)
        );
    }

}