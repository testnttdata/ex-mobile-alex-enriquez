package net.poc.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MenuPage {
    public static Target PRODUCTO =
            Target.the("Titulo PRODUCTS")
                    .located(By.xpath("\t\n" +
                            "//android.view.ViewGroup[@content-desc=\"test-Cart drop zone\"]/android.view.ViewGroup/android.widget.TextView"));
    public static Target ITEM =
            Target.the("Item en el menu")
                    .located(By.xpath("\t\n" +
                            "(//android.widget.TextView[@content-desc=\"test-Item title\"])[1]"));
}
