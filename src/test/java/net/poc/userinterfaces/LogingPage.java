package net.poc.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LogingPage {
    public static Target USER =
            Target.the("Ingreso de usuario")
                    .located(By.id("test-Username"));
    public static Target PASSWORD =
            Target.the("Ingreso contraseña")
                    .located(By.id("test-Username"));
    public static Target LOGIN_BTN =
            Target.the("Boton de loging")
                    .located(By.xpath("\t\n" +
                            "//android.view.ViewGroup[@content-desc=\"test-LOGIN\"]/android.widget.TextView"));
}

