package net.poc.questions;

import net.poc.userinterfaces.MenuPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ProductQuestions implements Question {

    @Override
    public String answeredBy(Actor actor) {
        return MenuPage.PRODUCTO.resolveFor(actor).getText();
    }
    public static Question<String> value(){
        return new ProductQuestions();
    }
}
