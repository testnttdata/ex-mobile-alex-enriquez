package stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.poc.questions.ItemQuestion;
import net.poc.questions.ProductQuestions;
import net.poc.tasks.LoginToApp;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class LogingStepDefinitions {
    @Managed(driver = "Appium")
    public WebDriver driver;

    @Before
    public void set_the_stage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^(.*) wants to log in$")
    public void alex_wants_to_log_in(String actor) {
        theActorCalled(actor).can(BrowseTheWeb.with(driver));
        theActorCalled(actor).entersTheScene();
    }

    @When("^(.*) enter your username and password$")
    public void alex_enter_your_username_and_password(String actor) {
        theActorCalled(actor).attemptsTo(LoginToApp.login());
    }

    @Then("^(.*) must verify that the title PRODUCTS and the article$")
    public void alexMustVerifyThatTheTitlePRODUCTSAndTheArticle(String actor) {
        String expectedProductText = "PRODUCTS";
        theActorInTheSpotlight().should(
                seeThat("Titulo de Products", ProductQuestions.value(), equalTo(expectedProductText))
        );

        String expectedItemText = "Sauce Labs Backpack";
        theActorInTheSpotlight().should(
                seeThat("Enunciado del ITEM", ItemQuestion.value(), equalTo(expectedItemText))
        );
    }
}
